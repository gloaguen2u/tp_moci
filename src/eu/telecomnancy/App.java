package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.sensor.AdapterSensor;
import eu.telecomnancy.ui.ConsoleUI;

public class App {

    public static void main(String[] args) {
        LegacyTemperatureSensor lsensor = new LegacyTemperatureSensor();
        ISensor sensor = new AdapterSensor(lsensor);
        new ConsoleUI(sensor);
    }

}