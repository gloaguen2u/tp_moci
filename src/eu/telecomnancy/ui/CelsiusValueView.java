package eu.telecomnancy.ui;

import java.awt.Font;

import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class CelsiusValueView extends ValueView {
	public CelsiusValueView() {
		super("N/A °C");
        setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        setFont(sensorValueFont);
	}
	
	@Override
	public void setTemperature(double t) {
		setText(t + "�C");
	}
}
