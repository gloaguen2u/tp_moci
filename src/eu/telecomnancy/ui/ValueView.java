package eu.telecomnancy.ui;

import javax.swing.JLabel;

@SuppressWarnings("serial")
public abstract class ValueView extends JLabel {

	public ValueView(String str) {
		super(str);
	}

	public abstract void setTemperature(double value);
	
}
