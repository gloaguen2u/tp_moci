package eu.telecomnancy.ui;

import java.awt.Font;

import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class FahrenheitValueView extends ValueView {
	public FahrenheitValueView() {
		super("N/A °F");
        setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        setFont(sensorValueFont);
	}
	
	@Override
	public void setTemperature(double t) {
		setText((1.8 * t + 32) + "�F");
	}
}
