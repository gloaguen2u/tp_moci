package eu.telecomnancy.ui;

import java.awt.Font;

import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class RoundedCValueView extends ValueView {
	public RoundedCValueView() {
		super("N/A °C");
        setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        setFont(sensorValueFont);
	}
	
	@Override
	public void setTemperature(double t) {
		Double tc = t;
		Integer i = tc.intValue();
		setText(i + "�C");
	}
}
