package eu.telecomnancy.sensor.command;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import eu.telecomnancy.helpers.ReadPropertyFile;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.ui.ValueView;

public class Invoker {
	private ArrayList<Command> commandQueue = new ArrayList<Command>();
	private ISensor sensor;
	private ValueView view;
	
	public Invoker(ISensor s, ValueView v) {
		this.sensor = s;
		this.view = v;
	}
	
	public void registerCommand(Command com) {
		commandQueue.add(com);
	}
	
	public void registerCommand(String str) {
        ReadPropertyFile rp=new ReadPropertyFile();
        Properties p;
		try {
			p = rp.readFile("/eu/telecomnancy/commande.properties");
	        String className = p.getProperty(str);
	        Command command = (Command) Class.forName(className).newInstance();;
	        registerCommand(command);
		} catch (IOException e) {
			System.err.println("Error while reading command.properties : ");
			e.printStackTrace();
		} catch (Exception e) {
			System.err.println("Error while executing unknown command : ");
			e.printStackTrace();
		}
	}
	
	public void executeCommands() {
		for(Command com : commandQueue) {
			com.execute(sensor, view);
		}
	}
}
