package eu.telecomnancy.sensor.command;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.ui.ValueView;

public class OnCommand extends Command {

	@Override
	public void execute(ISensor s, ValueView v) {
		s.on();
	}

}
