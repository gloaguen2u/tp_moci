package eu.telecomnancy.sensor.command;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.ui.ValueView;

public class GetValueCommand extends Command {

	public void execute(ISensor s, ValueView v) {
		try {
			v.setTemperature(s.getValue());
		} catch (SensorNotActivatedException e) {
			System.err.println("Error while getting value : ");
			e.printStackTrace();
		}
	}

}
