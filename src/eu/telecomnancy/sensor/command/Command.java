package eu.telecomnancy.sensor.command;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.ui.ValueView;

public abstract class Command {
	public abstract void execute(ISensor s, ValueView v);
}
