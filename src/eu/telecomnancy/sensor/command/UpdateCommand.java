package eu.telecomnancy.sensor.command;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.ui.ValueView;

public class UpdateCommand extends Command {

	public void execute(ISensor s, ValueView v) {
		try {
			s.update();
		}
		catch(Exception e) {
			System.err.println("Error while updating sensor : ");
			e.printStackTrace();
		}
		
	}

}
