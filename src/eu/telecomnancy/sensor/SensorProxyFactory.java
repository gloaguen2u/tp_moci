package eu.telecomnancy.sensor;

import java.io.IOException;
import java.util.Properties;

import eu.telecomnancy.helpers.ReadPropertyFile;

/**
 * Created with IntelliJ IDEA.
 * User: charoy
 * Date: 13/12/13
 * Time: 19:33
 */
public class SensorProxyFactory extends SensorFactory {
    @Override
    public ISensor getSensor() {
    	ReadPropertyFile rp = new ReadPropertyFile();
        Properties p = null;
        try {
            p = rp.readFile("/eu/telecomnancy/app.properties");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String factory = p.getProperty("sensor");
        ISensor sensor;
        sensor = null;
        try {
            sensor = (ISensor) Class.forName(factory).newInstance();

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    	
        return new SensorProxy(sensor, new SimpleSensorLogger());
    }
}
