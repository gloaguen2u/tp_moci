package eu.telecomnancy.sensor;

public class AdapterSensor implements ISensor {

	private LegacyTemperatureSensor link;
	private double lastUpdate;
	
	public AdapterSensor(LegacyTemperatureSensor linked) {
		this.link = linked;
	}
	
	@Override
	public void on() {
		if(!link.getStatus())
			link.onOff();
	}

	@Override
	public void off() {
		if(link.getStatus())
			link.onOff();
	}

	@Override
	public boolean getStatus() {
		return link.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		if(!link.getStatus())
			throw new SensorNotActivatedException("Senseur non activ�");
		this.lastUpdate = link.getTemperature();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		if(!link.getStatus())
			throw new SensorNotActivatedException("Senseur non activ�");
		return this.lastUpdate;
	}

}
