package eu.telecomnancy.sensor.StateSensor;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public class OffStateTemperatureSensor extends StateTemperatureSensor {

	@Override
	public void on() {
		// NO OP
	}

	@Override
	public void off() {
		// NO OP
	}

	@Override
	public boolean getStatus() {
		return false;
	}

	@Override
	public void update() throws SensorNotActivatedException {
		throw new SensorNotActivatedException("Senseur non activ�");
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		throw new SensorNotActivatedException("Senseur non activ�");
	}

}
