package eu.telecomnancy.sensor.StateSensor;

import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.TemperatureSensor;

public class OnStateTemperatureSensor extends StateTemperatureSensor {
	
	TemperatureSensor tsens;
	
	public OnStateTemperatureSensor() {
		tsens = new TemperatureSensor();
		tsens.on();
	}

	@Override
	public boolean getStatus() {
		return true;
	}

	@Override
	public void update() throws SensorNotActivatedException {
		tsens.update();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return tsens.getValue();
	}

	@Override
	public void on() {
		// NO OP
	}

	@Override
	public void off() {
		// NO OP
	}
	
	

}
