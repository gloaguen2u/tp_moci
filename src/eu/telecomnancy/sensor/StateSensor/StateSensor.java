package eu.telecomnancy.sensor.StateSensor;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class StateSensor implements ISensor {

	StateTemperatureSensor state;
	
	public StateSensor() {
		off();
	}
	
	@Override
	public void on() {
		state = new OnStateTemperatureSensor();
	}

	@Override
	public void off() {
		state = new OffStateTemperatureSensor();
	}

	@Override
	public boolean getStatus() {
		return state.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		state.update();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return state.getValue();
	}

}
