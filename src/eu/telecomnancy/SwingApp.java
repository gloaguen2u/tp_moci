package eu.telecomnancy;

import eu.telecomnancy.sensor.SensorFactory;
import eu.telecomnancy.ui.MainWindow;

public class SwingApp {

    public static void main(String[] args) {
        new MainWindow(SensorFactory.makeSensor());
    }

}
